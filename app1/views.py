from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from app1.models import *
import sys
from django.http import HttpResponse


# Create your views here.

def index(request):
    """各実験ページへのリンク。"""
    data = {}
    return render(request, 'app1/index.tpl', data)


def static_image(request):
    """ふつうにstatic画像を表示するページ。"""
    data = {}
    return render(request, 'app1/static_image.tpl', data)


def static_image_dynamic(request):
    """動的にstatic画像を表示するページ。"""
    data = {
        'png_path': 'app1/images/floare.png',
    }
    return render(request, 'app1/static_image_dynamic.tpl', data)


def htmlnized(request):
    """マークダウンモデルの最初のレコードをHTML化して表示するページ。"""
    data = {
        'row': MD.objects.all().first(),
    }
    return render(request, 'app1/htmlnized.tpl', data)


def htmlnized_github(request):
    """↑と同じ内容をgithub cssで装飾して表示するページ。"""
    data = {
        'row': MD.objects.all().first(),
    }
    return render(request, 'app1/htmlnized_github.tpl', data)


def static_karl(request):
    data = {
        'title': 'karl_test'
    }
    return render(request, 'app1/static_karl.tpl', data)


def page_server_error(request, *args, **kw):

    # Write error info in /var/log/httpd/error_log
    # import traceback
    # print(traceback.format_exc())

    from django.views import debug
    error_html = debug.technical_500_response(request, *sys.exc_info()).content
    return HttpResponse(error_html)
