from django.db import models
from markdownx.models import MarkdownxField
from markdownx.utils import markdownify

# Create your models here.


class MD(models.Model):
    markdown_text = MarkdownxField(
        verbose_name='本文',
        help_text='Markdown形式で書く。',
        null=True,
    )

    def text_to_markdown(self):
        return markdownify(self.markdown_text)
