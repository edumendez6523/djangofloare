{% load static %}

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>{{ title }}</title>
</head>
<style type="text/css">
    .karl{
        background-image: url("{% static 'app1/images/nature.jpg' %}");
        background-repeat: no-repeat;
        background-position: center center;
        background-attachment: fixed;
        background-size: cover;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
    }
</style>
<body class="karl">
  <h1>Test Karl</h1>
</body>

</html>