"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static  # Manually added.
from django.conf import settings
from app1 import views
from django.conf.urls import handler500

urlpatterns = [
    path('admin/', admin.site.urls),

    path(''                      , views.index                ),
    path('static_image/'         , views.static_image         ),
    path('static_image_dynamic/' , views.static_image_dynamic ),
    path('htmlnized/'            , views.htmlnized            ),
    path('htmlnized_github/'     , views.htmlnized_github     ),
    path('markdownx/'            , include('markdownx.urls')  ),
    path('karl_test/'            , views.static_karl          ),
]

# 開発環境でのメディアファイルの配信設定
urlpatterns += static(
    settings.MEDIA_URL,
    document_root=settings.MEDIA_ROOT,
)

handler500 = views.page_server_error
