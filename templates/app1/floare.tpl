
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <title>Floare - {{floare.title}}</title>
</head>

<body>
  <ul>
    <li>{{floare.title}}</li>
    <li>{{floare.tag.name}}</li>
  </ul>
  <p>
    {{floare.body}}
  </p>
</body>

</html>
