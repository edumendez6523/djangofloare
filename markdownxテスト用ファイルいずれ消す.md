meta1: めためた
       めためためた
meta2: メタメタ
       メタメタメタ
meta3: metameta
       metametameta

[TOC]

よく書くマークダウン

## リスト系

- リスト
    - リスト
        - リスト

見出し
:   内容
:   これはあんまり整わないから使わないかもしれん。


## 画像

![](/media/markdownx/a569469c-29a5-47da-b8bc-22a189ff7197.png)


## コード

```python
def foo():
    print('FOOFOO')
```


## 色を変える

<span style="color:red;">みー</span>、つぃー、にゃー


## テーブル

|  ひと  |   発言   |
|--------|----------|
| わたし | ちわっす |
| きみ   | どもっす |


## attr_list を使う

[公式 https://python-markdown.github.io/extensions/attr_list/](https://python-markdown.github.io/extensions/attr_list/)

<style>
  #red   { color:red  ; }
  .green { color:green; }
</style>

RED.
{: #red }

```plaintext
RED.
{: #red }
```

**GREEN.**{: .green}

```plaintext
**GREEN.**{: .green}
```
